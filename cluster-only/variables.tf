variable "env" {
    default = "adhoc-test"
}

variable "ec2_key_name" {}
variable "ec2_instance_type" {}
