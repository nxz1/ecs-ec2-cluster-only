output "vpc_id" {
    value = "${module.networking.vpc_id}"
}

output "public_subnets" {
    value = "${module.networking.public_subnets}"
}

output "private_subnets" {
    value = "${module.networking.private_subnets}"
}

output "nat_public_ips" {
    value = "${module.networking.nat_public_ips}"
}

output "instance_role_name" {
    value = "${module.iam.instance_profile_role_name}"
}

output "alb_dns" {
    value = "${module.alb.alb_dns}"
}

output "ecs_cluster_name" {
    value = "${module.ecs_cluster.ecs_cluster_name}"
}

output "ecs_asg_arn" {
    value = "${module.asg.ecs_asg_arn}"
}