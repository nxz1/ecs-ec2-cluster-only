module "networking" {
    env = "${var.env}"
    source = "../modules/networking"
    vpc_name = "${var.env}-vpc"
    vpc_cidr = "192.168.0.0/16"
    az1_name = "us-east-1a"
    az2_name = "us-east-1b"
    az3_name = "us-east-1c"
    private_subnet_1 = "192.168.1.0/24"
    private_subnet_2 = "192.168.2.0/24"
    private_subnet_3 = "192.168.3.0/24"
    public_subnet_1  = "192.168.101.0/24"
    public_subnet_2  = "192.168.102.0/24"
    public_subnet_3  = "192.168.103.0/24"
}

module "iam" {
    source = "../modules/iam"
    ecs_instance_role_name = "${var.env}-ecsInstanceRole"
}

module "alb" {
    env_name = "${var.env}"
    source = "../modules/alb"
    loadbalancer_name = "${var.env}-loadbalancer"
    subnets = "${module.networking.public_subnets}"
    loadbalancer_sg_name = "${var.env}-loadbalancer-security-group"
    vpc_id = "${module.networking.vpc_id}"
    #service_1_TG_name = "${var.env}-webapp-target-group"
}

module "asg" {
    source = "../modules/asg"
    ecs_instance_sg_name = "${var.env}-ecs-instance-security-group"
    ec2_vpc_id = "${module.networking.vpc_id}"
    ecs_instance_lc_name = "${var.env}-ecs-launch-configuration"
    iam_instance_profile = "${module.iam.instance_profile_role_name}"
    ecs_instance_key_name = "${var.ec2_key_name}"
    ecs_cluster_name = "${var.env}-ecs-cluster"
    ecs_asg_name = "${var.env}-ecs-autoscaling-group"
    vpc_zone_identifier = "${module.networking.public_subnets}"
    instance_type = "${var.ec2_instance_type}"
    # service_1_target_group_arn = "${module.alb.service_1_target_group_arn}"
}

module "ecs_cluster" {
    source = "../modules/ecs_cluster"
    ecs_cluster_name = "${var.env}-ecs-cluster"
    container_insights = "enabled"
    cw_log_group_name = "/ecs/${var.env}-ecs-cluster-logging"

    capacity_provider_name = "custom-ecs-capacity-provider"
    asg_arn = "${module.asg.ecs_asg_arn}"
    managed_termination_protection = "DISABLED"
    managed_scaling = "ENABLED"
}
