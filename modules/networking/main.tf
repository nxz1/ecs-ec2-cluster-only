# resource "aws_eip" "nat" {
#   count = 3
#   vpc = true
# }

module "vpc" {
    source = "terraform-aws-modules/vpc/aws"
    name = "${var.vpc_name}"
    cidr = "${var.vpc_cidr}"
    public_subnets  = ["${var.public_subnet_1}", "${var.public_subnet_2}", "${var.public_subnet_3}"]
    azs             = ["${var.az1_name}", "${var.az2_name}", "${var.az3_name}"]
    
    # private_subnets = ["${var.private_subnet_1}", "${var.private_subnet_2}", "${var.private_subnet_3}"]
    # enable_nat_gateway = true  # Using this parameter, the Elastic IP will be release if stack is destroyed.
    # single_nat_gateway  = false
    # reuse_nat_ips       = true                    # <= Skip creation of EIPs for the NAT Gateways
    # external_nat_ip_ids = "${aws_eip.nat.*.id}"   # <= IPs specified here as input to the module

  tags = {
    Terraform = "true"
    Environment = "${var.env}"
  }
}
