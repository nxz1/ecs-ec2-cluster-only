  variable "env" {
      default = "default-adhoc-terraform-environment"
  }
  variable "vpc_name" {
      default = "dev-vpc"
  }
  variable "vpc_cidr" {
      default = "200.100.0.0/16"
  }
  variable "az1_name" {
      default = "us-east-1a"
  }
  variable "az2_name" {
      default = "us-east-1b"
  }
  variable "az3_name" {
      default = "us-east-1c"
  }
  variable "private_subnet_1" {
      default = "200.100.10.0/24"
  }
  variable "private_subnet_2" {
      default = "200.100.20.0/24"
  }
  variable "private_subnet_3" {
      default = "200.100.30.0/24"
  }
  variable "public_subnet_1" {
      default = "200.100.160.0/24"
  }
  variable "public_subnet_2" {
      default = "200.100.170.0/24"
  }
  variable "public_subnet_3" {
      default = "200.100.180.0/24"
  }