variable "ecs_cluster_name" {}
variable "container_insights" {}
variable "cw_log_group_name" {}

variable "capacity_provider_name" {}
variable "asg_arn" {}
variable "managed_termination_protection" {}
variable "managed_scaling" {}