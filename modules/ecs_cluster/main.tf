resource "aws_ecs_cluster" "my_cluster" {
  name = "${var.ecs_cluster_name}"

  setting {
    name  = "containerInsights"
    value = "${var.container_insights}"
  }
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "${var.cw_log_group_name}"
  tags = {
    "env"       = "${var.cw_log_group_name}"
  }
}

resource "aws_ecs_capacity_provider" "my_capacity_provider" {
  name = "${var.capacity_provider_name}"
  auto_scaling_group_provider {
    auto_scaling_group_arn         = "${var.asg_arn}"
    managed_termination_protection = "${var.managed_termination_protection}"

    managed_scaling {
      status          = "${var.managed_scaling}"
      target_capacity = 85
    }
  }
}
