variable "env" {
    default = "dev"
}

variable "ecs_instance_sg_name" {
    default = "ecs-security-group"
}

variable "ec2_vpc_id" {}

variable "ecs_instance_lc_name" {
    default = "ecs-instance-launch-configuration"
}

variable "iam_instance_profile" {}

variable "ecs_instance_key_name" {}

variable "ecs_cluster_name" {}

variable "ecs_asg_name" {
    default = "ecs-auto-scaling-group"
}
variable "vpc_zone_identifier" {}

variable "instance_type" {}

# variable "service_1_target_group_arn" {}