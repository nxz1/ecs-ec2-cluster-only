variable "env_name" {
    default = "adhoc"
}
variable "loadbalancer_name" {
    default = "dev-loadbalancer"
}
variable "subnets" {
    type = list(string)
}
variable "loadbalancer_sg_name" {}
variable "vpc_id" {}
# variable "service_1_TG_name" {}
