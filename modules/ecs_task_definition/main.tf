resource "aws_ecs_task_definition" "task_definition_1" {
  family                = "${var.task_definition_family_name}"
  container_definitions = file("${var.task_definition_container_def_path}")
  network_mode          = "bridge"
  tags = {
    "env"       = "${var.env}"
  }
}