output "task_definition_name" {
    value = "${aws_ecs_task_definition.task_definition_1.family}"
}

output "task_definition_arn" {
    value = "${aws_ecs_task_definition.task_definition_1.arn}"
}