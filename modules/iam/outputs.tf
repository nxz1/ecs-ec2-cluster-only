output "instance_profile_role_name" {
    value = "${aws_iam_instance_profile.ecs_service_role.name}"
}