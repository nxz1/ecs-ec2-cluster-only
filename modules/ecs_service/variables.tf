variable "listener_arn" {}

variable "ecs_cluster_id" {}
variable "service_name" {}
variable "task_definition_arn" {}

variable "service_target_group_arn" {}
variable "service_container_name" {}
variable "service_container_port" {}