data "aws_lb_listener" "web_listener" {
  arn = "${var.listener_arn}"
}

resource "aws_ecs_service" "service_1" {
  name            = "${var.service_name}"
  cluster         = "${var.ecs_cluster_id}"
  task_definition = "${var.task_definition_arn}"
  enable_ecs_managed_tags = true
  propagate_tags = "SERVICE"
  desired_count   = 1
  ordered_placement_strategy {
    type  = "spread"
    field = "host"
  }
  load_balancer {
    target_group_arn = "${var.service_target_group_arn}"
    container_name   = "${var.service_container_name}"
    container_port   = "${var.service_container_port}"
  }
  # Optional: Allow external changes without Terraform plan difference(for example ASG)
  lifecycle {
    ignore_changes = [desired_count]
  }
  launch_type = "EC2"
  depends_on = [data.aws_lb_listener.web_listener]
}