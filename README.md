# Vanilla Terraform for ECS EC2 Cluster

#### REQUIRED ENVIRONMENT VARIABLES THAT SHOULD BE UPDATED UNDER tfvars
1. aws_profile  # aws cli profile name
2. aws_region # aws region for ecs
3. ec2_instance_type # instance type for ecs
3. key_name # key name for ecs instances (SHOULD BE CREATED BEFORE HAND)


### Get Started
#### STEP 1: Login to AWS and create a key pair from EC2 console.
#### STEP 2: Update Environment File
    $ cd environments/dev
    $ vim dev.tfvars

#### STEP 3: Run terraform commands
    terraform init
    terraform plan --var-file dev.tfvars
    terraform apply --var-file dev.tfvars

