env = "dev" # Optional.... If no value specified, defaults to dev.
ec2_instance_type = "t2.micro"
key_name = "demo" # Your key name
aws_region = "us-east-1" # Your AWS region
aws_profile = "default" # Your AWS profile
