output "vpc_id" {
    value = "${module.cluster-only.vpc_id}"
}

output "public_subnets" {
    value = "${module.cluster-only.public_subnets}"
}

output "private_subnets" {
    value = "${module.cluster-only.private_subnets}"
}

output "nat_public_ips" {
    value = "${module.cluster-only.nat_public_ips}"
}

output "instance_role_name" {
    value = "${module.cluster-only.instance_role_name}"
}

output "alb_dns_name" {
    value = "${module.cluster-only.alb_dns}"
}

output "ecs_cluster_name" {
    value = "${module.cluster-only.ecs_cluster_name}"
}