provider "aws" {
    region = "${var.aws_region}"
    profile = "${var.aws_profile}"
}

 terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

module "cluster-only" {
    source = "../../cluster-only"
    env = "${var.env}"
    ec2_key_name = "${var.key_name}"
    ec2_instance_type = "${var.ec2_instance_type}"
}