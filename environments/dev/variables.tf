variable "env" {
    default = "dev"
}

variable "key_name" {
    description = "The name of the key pair to login to EC2 Instances. (KEY MUST BE PRESENT IN AWS REGION MENTIONED!)"
}

variable "aws_profile" {
    description = "AWS Profile to be used by terraform"
}

variable "aws_region" {
    description = "AWS Region for ECS cluster"
}

variable "ec2_instance_type" {}